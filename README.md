# Python Template Environment

Python with poetry over Docker

## Ref

- [個人的Python開発環境構築 - NIFTY engineering](https://engineering.nifty.co.jp/blog/16673)
- [docker-composeを使う上でホストとコンテナのユーザーIDとグループIDを揃える - Qiita](https://qiita.com/ma-me/items/c80f7f8bf9a61cbd21f7)
