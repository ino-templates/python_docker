.PHONY: build
build:
	docker compose build --build-arg UID="$$(id -u)" --build-arg GID="$$(id -g)" --build-arg USERNAME="$$(whoami)" --build-arg GROUPNAME="$$(whoami)" && docker-compose up -d
